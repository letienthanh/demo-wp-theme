<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HLGR
 */

$sections = get_field('sections','option');
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<?php echo wpautop($sections['footer']); ?>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

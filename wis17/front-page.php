<?php
get_header();

    while ( have_posts() ) : 
        the_post(); ?>

        <?php 
        $sections = get_field('sections','option');

        $partner = $sections['partners'];
        $partner_desc = $partner['desc'];
        $partner_items = $partner['items']; ?>

        <section id="partners" class="section">
            <div class="s-inner">
                <h1><?php _e('Partners','wis17'); ?></h1>
                <div class="s-desc">
                    <?php echo wpautop($partner_desc); ?>
                </div>
            </div>
            
            <?php if($partner_items){ ?>
            <div class="owl-carousel">
                <?php
                $partners = array_chunk($partner_items,2);
                $index = 0;
                foreach($partners as $items){ ?>
                <div class="item">
                    <?php foreach($items as $key=>$item){ ?>
                    <div class="partner-item table">
                    <?php if($key % 2 != 0){ ?>
                        <div class="logo table-cell">
                            <a href="<?php echo $item['link']?>" target="_blank">
                                <img src="<?php echo $item['logo']['sizes']['medium']; ?>" />
                            </a>
                        </div>
                        <?php 
                        $gallery = $item['gallery']; 
                        if($gallery){ ?>
                        <div class="gal table-cell">
                            <div class="featured" data-popup="popup-partners" data-key="<?php echo $index; ?>" style="background-image:url(<?php echo $gallery[0]['sizes']['square-big']; ?>);">
                                <div style="background-color: <?php echo hex2rgba($item['color'],0.35); ?>;"></div>
                            </div>
                        </div>
                        <?php 
                        } 
                    } else { ?>
                        <?php 
                        $gallery = $item['gallery']; 
                        if($gallery){ ?>
                        <div class="gal table-cell">
                            <div class="featured" data-popup="popup-partners" data-key="<?php echo $index; ?>" style="background-image:url(<?php echo $gallery[0]['sizes']['square-big']; ?>);">
                                <div style="background-color: <?php echo hex2rgba($item['color'],0.35); ?>;"></div>
                            </div>
                        </div>
                        <?php 
                        } ?>
                        <div class="logo table-cell">
                            <a href="<?php echo $item['link']?>" target="_blank">
                                <img src="<?php echo $item['logo']['sizes']['medium']; ?>" />
                            </a>
                        </div>
                    <?php 
                    } ?>
                    </div>
                    <?php $index++; } ?>
                </div>
                <?php } ?>
            </div>
            <?php } ?>

            <?php
            $map = $sections['map'];
            if($map){
                $map_desktop = $map['desktop']['url'];
                $map_mobile = (!empty($map['mobile'])) ? $map['mobile']['url'] : $map_desktop;
            ?>
            <div class="map">
                <img class="large" src="<?php echo $map_desktop; ?>" />
                <img class="small" src="<?php echo $map_mobile; ?>" />
            </div>
            <?php 
            } ?>

            <?php if($partner_items){ ?>
            <div id="popup-partners" class="popup">
                <div class="topbar clear">
                    <div class="x"></div>
                    <div class="nav">
                        <span class="next"><?php _e('Next','wis17'); ?></span>
                        <span class="prev"><?php _e('Previous','wis17'); ?></span>
                    </div>
                </div>
                <div class="popup-content">
                    <div class="owl-carousel">
                        <?php 
                        foreach($partner_items as $item){
                            $gallery = $item['gallery']; ?>
                        <div class="item">
                            <div class="item-content">
                                <?php if(sizeof($gallery) > 0){ array_shift($gallery); ?>
                                <div class="table">
                                    <div class="table-cell slides">
                                        <div class="cycleslideshow" data-cycle-speed=1500 data-cycle-timeout=2500 data-cycle-slides=">img">
                                            <?php foreach($gallery as $img){ ?>
                                            <img src="<?php echo $img['sizes']['square-big']; ?>" alt="">
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="table-cell detail">
                                        <div class="partner-logo">
                                            <a href="<?php echo $item['link']?>" target="_blank">
                                                <img src="<?php echo $item['logo']['sizes']['medium']; ?>" />
                                            </a>
                                        </div>
                                        <div class="highlight" style="color:<?php echo $item['color']; ?>">
                                            <?php echo wpautop($item['highlight']);?>
                                        </div>
                                        <div class="desc">
                                            <?php echo wpautop($item['desc']);?>
                                        </div>
                                        <a class="button" href="<?php echo $item['link']?>" target="_blank"><?php _e('Website','wis17'); ?></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="pattern">
                                <?php echo $item['illustration']; ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>

        </section>

        <section id="routes" class="section">
            <div class="s-inner">
                <h1><?php _e('Routes','wis17'); ?></h1>
                <div class="s-content">
                    <?php 
                    $routes = get_posts(array(
                        'posts_per_page' => -1,
                        'post_type' => 'route',
                        'suppress_filters' => false
                    ));
                    if($routes){
                        foreach($routes as $index=>$route){
                            $thumbnail = get_the_post_thumbnail_url($route->ID,'large');
                            $title = $route->post_title;
                            $link = get_permalink($route->ID); ?>
                            <div class="item">
                                <div class="img" style="background-image:url('<?php echo $thumbnail; ?>');"></div>
                                <div class="info">
                                    <span><?php echo __('Suggestion','wis17'); ?></span>
                                    <h2><?php echo $title; ?></h2>
                                    <a class="button" data-popup="popup-routes" data-key="<?php echo $index; ?>" href="#"><?php _e('More info','wis17'); ?></a>
                                </div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>

            <div id="popup-routes" class="popup">
                <div class="topbar clear">
                    <div class="x"></div>
                    <div class="nav">
                        <span class="next"><?php _e('Next','wis17'); ?></span>
                        <span class="prev"><?php _e('Previous','wis17'); ?></span>
                    </div>
                </div>
                <div class="popup-content">
                    <div class="owl-carousel">
                        <?php 
                        if($routes){
                            foreach($routes as $index=>$route){ 
                                $gal = get_field('gallery',$route->ID); 
                                $title = $route->post_title; ?>
                        <div class="item">
                            <div class="item-content">
                                <span><?php echo __('Suggestion','wis17'); ?></span>
                                <h2><?php echo $title; ?></h2>
                                <div class="route-detail">
                                    <?php echo wpautop($route->post_content); ?>
                                </div>
                            </div>
                            <?php if($gal){ ?>
                            <div class="gal cols-<?php echo sizeof($gal); ?>">
                            <?php foreach($gal as $img){
                                echo '<div style="background-image:url(' . $img['sizes']['600x420'] . ');" ></div>';
                            }?>
                            </div>
                            <?php } ?>
                        </div>
                        <?php }
                        } ?>
                    </div>
                </div>
            </div>

        </section>

        <section id="contact" class="section">
            <div class="s-inner">
                <h1><?php _e('Contact','wis17'); ?></h1>
                <div class="team clear">
                    <?php 
                    $team = $sections['contact'];
                    $team = $team['team'];
                    if($team){
                        foreach($team as $p){
                            $market = $p['market'];
                            $avatar = $p['avatar'];
                            $name = $p['name'];
                            $city = $p['city'];
                            $mobile = $p['mobile'];
                            $email = $p['email']; ?>
                            <div class="member">
                                <hr>
                                <?php if($market): ?>
                                <h2><span><?php echo $market; ?></span></h2>
                                <?php endif; ?>

                                <?php if($avatar): ?>
                                <img src="<?php echo $avatar['sizes']['square']; ?>"/>
                                <?php endif; ?>

                                <?php if(!empty($name)): ?>
                                <h3><?php echo $name;?></h3>
                                <?php endif; ?>

                                <?php if(!empty($city)): ?>
                                <h4><?php echo $city;?></h4>
                                <?php endif; ?>

                                <?php if(!empty($mobile)): ?>
                                <span><?php echo $mobile; ?></span>
                                <?php endif; ?>

                                <?php if(!empty($email)): ?>
                                <span><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></span>
                                <?php endif; ?>
                            </div>
                        <?php	
                        }
                    }
                    ?>
                </div>
                <div class="clear"></div>
            </div>
        </section>

    <?php
    endwhile;

get_footer(); ?>

<script>
    jQuery(document).ready(function () {
        //jQuery('.member').matchHeight();
        jQuery('.member h2 span').matchHeight();
    });
</script>
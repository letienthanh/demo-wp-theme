/**
 * File common.js.
 *
 */

/*
 * jquery-match-height 0.7.2 by @liabru
 * http://brm.io/jquery-match-height/
 * License MIT
 */
!(function (t) {
  "use strict";
  "function" == typeof define && define.amd
    ? define(["jquery"], t)
    : "undefined" != typeof module && module.exports
      ? (module.exports = t(require("jquery")))
      : t(jQuery);
})(function (t) {
  var e = -1,
    o = -1,
    n = function (t) {
      return parseFloat(t) || 0;
    },
    a = function (e) {
      var o = 1,
        a = t(e),
        i = null,
        r = [];
      return (
        a.each(function () {
          var e = t(this),
            a = e.offset().top - n(e.css("margin-top")),
            s = r.length > 0 ? r[r.length - 1] : null;
          null === s
            ? r.push(e)
            : Math.floor(Math.abs(i - a)) <= o
              ? (r[r.length - 1] = s.add(e))
              : r.push(e),
            (i = a);
        }),
        r
      );
    },
    i = function (e) {
      var o = {
        byRow: !0,
        property: "height",
        target: null,
        remove: !1
      };
      return "object" == typeof e
        ? t.extend(o, e)
        : ("boolean" == typeof e
          ? (o.byRow = e)
          : "remove" === e && (o.remove = !0),
          o);
    },
    r = (t.fn.matchHeight = function (e) {
      var o = i(e);
      if (o.remove) {
        var n = this;
        return (
          this.css(o.property, ""),
          t.each(r._groups, function (t, e) {
            e.elements = e.elements.not(n);
          }),
          this
        );
      }
      return this.length <= 1 && !o.target
        ? this
        : (r._groups.push({ elements: this, options: o }),
          r._apply(this, o),
          this);
    });
  (r.version = "0.7.2"),
    (r._groups = []),
    (r._throttle = 80),
    (r._maintainScroll = !1),
    (r._beforeUpdate = null),
    (r._afterUpdate = null),
    (r._rows = a),
    (r._parse = n),
    (r._parseOptions = i),
    (r._apply = function (e, o) {
      var s = i(o),
        h = t(e),
        l = [h],
        c = t(window).scrollTop(),
        p = t("html").outerHeight(!0),
        u = h.parents().filter(":hidden");
      return (
        u.each(function () {
          var e = t(this);
          e.data("style-cache", e.attr("style"));
        }),
        u.css("display", "block"),
        s.byRow &&
        !s.target &&
        (h.each(function () {
          var e = t(this),
            o = e.css("display");
          "inline-block" !== o &&
            "flex" !== o &&
            "inline-flex" !== o &&
            (o = "block"),
            e.data("style-cache", e.attr("style")),
            e.css({
              display: o,
              "padding-top": "0",
              "padding-bottom": "0",
              "margin-top": "0",
              "margin-bottom": "0",
              "border-top-width": "0",
              "border-bottom-width": "0",
              height: "100px",
              overflow: "hidden"
            });
        }),
          (l = a(h)),
          h.each(function () {
            var e = t(this);
            e.attr("style", e.data("style-cache") || "");
          })),
        t.each(l, function (e, o) {
          var a = t(o),
            i = 0;
          if (s.target) i = s.target.outerHeight(!1);
          else {
            if (s.byRow && a.length <= 1) return void a.css(s.property, "");
            a.each(function () {
              var e = t(this),
                o = e.attr("style"),
                n = e.css("display");
              "inline-block" !== n &&
                "flex" !== n &&
                "inline-flex" !== n &&
                (n = "block");
              var a = {
                display: n
              };
              (a[s.property] = ""),
                e.css(a),
                e.outerHeight(!1) > i && (i = e.outerHeight(!1)),
                o ? e.attr("style", o) : e.css("display", "");
            });
          }
          a.each(function () {
            var e = t(this),
              o = 0;
            (s.target && e.is(s.target)) ||
              ("border-box" !== e.css("box-sizing") &&
                ((o +=
                  n(e.css("border-top-width")) +
                  n(e.css("border-bottom-width"))),
                  (o += n(e.css("padding-top")) + n(e.css("padding-bottom")))),
                e.css(s.property, i - o + "px"));
          });
        }),
        u.each(function () {
          var e = t(this);
          e.attr("style", e.data("style-cache") || null);
        }),
        r._maintainScroll &&
        t(window).scrollTop((c / p) * t("html").outerHeight(!0)),
        this
      );
    }),
    (r._applyDataApi = function () {
      var e = {};
      t("[data-match-height], [data-mh]").each(function () {
        var o = t(this),
          n = o.attr("data-mh") || o.attr("data-match-height");
        n in e ? (e[n] = e[n].add(o)) : (e[n] = o);
      }),
        t.each(e, function () {
          this.matchHeight(!0);
        });
    });
  var s = function (e) {
    r._beforeUpdate && r._beforeUpdate(e, r._groups),
      t.each(r._groups, function () {
        r._apply(this.elements, this.options);
      }),
      r._afterUpdate && r._afterUpdate(e, r._groups);
  };
  (r._update = function (n, a) {
    if (a && "resize" === a.type) {
      var i = t(window).width();
      if (i === e) return;
      e = i;
    }
    n
      ? o === -1 &&
      (o = setTimeout(function () {
        s(a), (o = -1);
      }, r._throttle))
      : s(a);
  }),
    t(r._applyDataApi);
  var h = t.fn.on ? "on" : "bind";
  t(window)[h]("load", function (t) {
    r._update(!1, t);
  }),
    t(window)[h]("resize orientationchange", function (t) {
      r._update(!0, t);
    });
});

/*--------------------------------------------------------------
  # Functions define
  --------------------------------------------------------------*/

// Check screen breakpoint
function isBreakpoint(alias) {
  return $(".device-" + alias).is(":visible");
}

// Check is mobile
function is_mobile() {
  var isMobile = false;
  if (
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
      navigator.userAgent
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
      navigator.userAgent.substr(0, 4)
    )
  ) {
    isMobile = true;
  }
  return isMobile;
}

// Determine the mobile operating system
function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return "Windows Phone";
  }
  if (/android/i.test(userAgent)) {
    return "Android";
  }
  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }
  return "unknown";
}

// Scrollbar width
function getScrollBarWidth() {
  var inner = document.createElement("p");
  inner.style.width = "100%";
  inner.style.height = "200px";
  var outer = document.createElement("div");
  outer.style.position = "absolute";
  outer.style.top = "0px";
  outer.style.left = "0px";
  outer.style.visibility = "hidden";
  outer.style.width = "200px";
  outer.style.height = "150px";
  outer.style.overflow = "hidden";
  outer.appendChild(inner);
  document.body.appendChild(outer);
  var w1 = inner.offsetWidth;
  outer.style.overflow = "scroll";
  var w2 = inner.offsetWidth;
  if (w1 == w2) w2 = outer.clientWidth;
  document.body.removeChild(outer);
  return w1 - w2;
}

// Apply the animation for elemets while scrolling
function onScrollInit(items, trigger) {
  items.each(function () {
    var osElement = $(this),
      osAnimationClass = osElement.attr("data-os-animation"),
      osAnimationDuration = osElement.attr("data-os-animation-duration"),
      osAnimationDelay = osElement.attr("data-os-animation-delay");
    osAnimationOffset = osElement.attr("data-os-animation-offset");
    osElement.css({
      "-webkit-animation-delay": osAnimationDelay,
      "-moz-animation-delay": osAnimationDelay,
      "animation-delay": osAnimationDelay,
      "-webkit-animation-duration": osAnimationDuration,
      "animation-duration": osAnimationDuration
    });
    var osTrigger = trigger ? trigger : osElement;
    osTrigger.waypoint(
      function () {
        osElement.toggleClass("animated").toggleClass(osAnimationClass);
      },
      {
        triggerOnce: false,
        offset: osAnimationOffset != undefined ? osAnimationOffset : "95%"
      }
    );
  });
}

function parseVideoURL(url) {
  var retVal = {};
  var matches;
  var success = false;
  if (url.match("http(s)?://(www.)?youtube|youtu.be")) {
    if (url.match("embed")) {
      retVal.id = url.split(/embed\//)[1].split('"')[0];
    } else {
      retVal.id = url.split(/v\/|v=|youtu\.be\//)[1].split(/[?&]/)[0];
    }
    retVal.provider = "youtube";
    retVal.videoUrl =
      "https://www.youtube.com/embed/" +
      retVal.id +
      "?autoplay=1&mute=1&rel=0&showinfo=0&iv_load_policy=3";
    success = true;
  } else if ((matches = url.match(/vimeo.com\/(\d+)/))) {
    retVal.provider = "vimeo";
    retVal.id = matches[1];
    retVal.videoUrl =
      "https://player.vimeo.com/video/" +
      retVal.id +
      "?title=0&byline=0&portrait=0&autoplay=1&muted=1&quality=1080p";
    success = true;
  }
  if (success) {
    return retVal;
  } else {
    console.log("No valid media id detected");
  }
}

(function ($) {

  $(window).bind("scroll", function () {
    if ($(window).scrollTop() >= $(window).height() - 77) {
      $("#site-navigation").addClass("fixed");
      $(".full-video .player").empty().hide();
    } else {
      $("#site-navigation").removeClass("fixed");
    }
  });

  $(document).ready(function () {
    var sw = $(window).width();
    if (sw < 1024) {
      $('header video').removeAttr('autoplay');
    } else {
      $('header video').attr('autoplay');
    }

    $("#partners > .owl-carousel").owlCarousel({
      items: 5,
      loop: true,
      margin: 3,
      responsive: {
        0: {
          items: 1,
          autoplay: true,
          autoplayTimeout: 2000,
          autoplayHoverPause: true,
          autoplaySpeed: 800
        },
        575: {
          items: 2,
          autoplay: true,
          autoplayTimeout: 2000,
          autoplayHoverPause: true,
          autoplaySpeed: 800
        },
        768: {
          items: 3,
        },
      }
    });

    var owl_popup_partner = $("#popup-partners .owl-carousel").owlCarousel({
      items: 1,
      loop: true
    });

    var owl_popup_routes = $("#popup-routes .owl-carousel").owlCarousel({
      items: 1,
      loop: true
    });

    $(".popup .nav span").click(function (e) {
      var active_popup = $(".popup.active").attr("id");
      var el =
        active_popup == "popup-partners" ? owl_popup_partner : owl_popup_routes;
      if ($(this).is(".next")) {
        el.trigger("next.owl.carousel");
      } else {
        el.trigger("prev.owl.carousel");
      }
    });

    $(".menu-toggle").click(function (e) {
      $(".main-navigation").toggleClass("expanded");
    });

    $("#site-navigation .menu a").click(function (e) {
      e.preventDefault();
      var $this = $(this);
      var target = $(this).attr("href");
      var menu_height = $("#site-navigation").outerHeight();
      $("body,html").animate({
        scrollTop: $(target).offset().top - menu_height
      });
    });

    $(".popup .close").click(function (e) {
      $(this)
        .closest(".popup")
        .removeClass("active");
      if ($(".popup.active").length == 0) {
        $("body").removeClass("popup-open");
      }
    });

    $(".menu-toggle").click(function (e) {
      $("body").toggleClass("menu-open");
    });

    $("header .full-video").click(function (e) {
      var video_url = $(this).data("video");
      $(this)
        .children(".player")
        .html(
          '<iframe src="' +
          parseVideoURL(video_url).videoUrl +
          '" class="embed-responsive-item allowfullscreen" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
        )
        .fadeIn();
    });

    $("[data-popup]").click(function (e) {
      e.preventDefault();
      var popup = $(this).data("popup");
      var index = $(this).data("key");
      if (popup == "popup-partners") {
        owl_popup_partner.trigger("to.owl.carousel", [index, 0]);
      }
      if (popup == "popup-routes") {
        owl_popup_routes.trigger("to.owl.carousel", [index, 0]);
      }
      $("#" + popup).addClass("active");
      $("#" + popup).find('.cycleslideshow').cycle();
      $("body").addClass("popup-open");
    });

    $(".popup .x").click(function (e) {
      $(".popup.active").addClass("fadingOut");
      setTimeout(function () {
        $(".popup.active").removeClass("active fadingOut");
        $("body").removeClass("popup-open");
      }, 800);
    });

    $(".section").waypoint(
      function (direction) {
        if (direction === "up") {
          var $this = $(this.element);
          var loaded_id = $this.attr("id");
          $("ul.menu li").removeClass("active");
          $("ul.menu a[href$='" + loaded_id + "']")
            .parents("li")
            .addClass("active");
        }
      },
      {
        offset: "-50%"
      }
    );

    $(".section").waypoint(
      function (direction) {
        if (direction === "down") {
          var $this = $(this.element);
          var loaded_id = $this.attr("id");
          $("ul.menu li").removeClass("active");
          $("ul.menu a[href$='" + loaded_id + "']")
            .parents("li")
            .addClass("active");
        }
      },
      {
        offset: "50%"
      }
    );

    (function () {
      $('.popup .item').each(function () {
        var _overlay = $(this).get(0);
        var _clientY = null; // remember Y position on touch start

        _overlay.addEventListener(
          "touchstart",
          function (event) {
            if (event.targetTouches.length === 1) {
              // detect single touch
              _clientY = event.targetTouches[0].clientY;
            }
          },
          false
        );

        _overlay.addEventListener(
          "touchmove",
          function (event) {
            if (event.targetTouches.length === 1) {
              // detect single touch
              disableRubberBand(event);
            }
          },
          false
        );

        function disableRubberBand(event) {
          var clientY = event.targetTouches[0].clientY - _clientY;

          if (_overlay.scrollTop === 0 && clientY > 0) {
            // element is at the top of its scroll
            event.preventDefault();
          }

          if (isOverlayTotallyScrolled() && clientY < 0) {
            //element is at the top of its scroll
            event.preventDefault();
          }
        }

        function isOverlayTotallyScrolled() {
          // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
          return (
            _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight
          );
        }
      });
    })();

  });
})(jQuery);
